%global user_name               sys_acme

%global dir_home                /home
%global dir_users               %{dir_home}/user
%global dir_storage             %{dir_home}/storage

%global dir_data                %{dir_storage}/data/data.00
%global dir_acme                %{dir_data}/acme

%global dir_bin                 %{_prefix}/local/bin

Name:                           meta-acme
Version:                        1.0.1
Release:                        3%{?dist}
Summary:                        META-package for install ACME
License:                        MIT

Requires:                       openssl curl sed zsh meta-system

%description
META-package for install ACME.

# -------------------------------------------------------------------------------------------------------------------- #
# -----------------------------------------------------< SCRIPT >----------------------------------------------------- #
# -------------------------------------------------------------------------------------------------------------------- #

%pre
# Install: users.
for i in %{user_name}; do
    getent group ${i} >/dev/null || groupadd ${i}
    getent passwd ${i} >/dev/null || \
        useradd -g ${i} -d %{dir_users}/${i} -s /bin/zsh \
        -c "System ACME account" ${i}
done

exit 0

%install
# Install: ACME storage.
for i in certs accounts challenges chains; do
    %{__mkdir} -p %{buildroot}%{dir_acme}/${i}
done

%files
# ACME directories.
%attr(0755,%{user_name},%{user_name}) %dir %{dir_acme}
%attr(0755,%{user_name},%{user_name}) %dir %{dir_acme}/certs
%attr(0700,%{user_name},%{user_name}) %dir %{dir_acme}/accounts
%attr(0755,%{user_name},%{user_name}) %dir %{dir_acme}/challenges
%attr(0700,%{user_name},%{user_name}) %dir %{dir_acme}/chains


%changelog
* Fri Apr 19 2019 Kitsune Solar <kitsune.solar@gmail.com> - 1.0.1-3
- Change directory permissions.

* Fri Apr 19 2019 Kitsune Solar <kitsune.solar@gmail.com> - 1.0.1-2
- Change ACME user.
- Remove scripts.

* Tue Apr 16 2019 Kitsune Solar <kitsune.solar@gmail.com> - 1.0.1-1
- Add user configuration.

* Sat Mar 30 2019 Kitsune Solar <kitsune.solar@gmail.com> - 1.0.0-3
- New version: 1.0.0-3.

* Sat Mar 30 2019 Kitsune Solar <kitsune.solar@gmail.com> - 1.0.0-2
- New version: 1.0.0-2.

* Mon Mar 25 2019 Kitsune Solar <kitsune.solar@gmail.com> - 1.0.0-1
- Initial build.
