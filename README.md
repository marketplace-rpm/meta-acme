# Information / Информация

SPEC-файл для создания RPM-пакета **meta-acme**.

## Install / Установка

1. Подключить репозиторий **MARKETPLACE**: `dnf copr enable marketplace/common-meta`.
2. Установить пакет: `dnf install meta-acme`.

## Support / Поддержка

- [Forum](https://unix.community/)
- [IRC](irc://irc.freenode.net/marketplace-fedora)
- [Documentation](https://sysadmins.wiki/)
- [Code of Conduct](https://metainfo.github.io/coc/)
- [Contributing](https://metainfo.github.io/contributing/)

## Donation / Пожертвование

- [Liberapay](https://liberapay.com/marketplace/donate)
- [Patreon](https://patreon.com/marketplace)